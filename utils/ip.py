from ipaddress import ip_address


def nargs_ips(ip_list: list) -> list:
    """
    Takes a list in the form of ["ip:port", "ip2:port2", ...]
    and converts it into a proper list of ip, address tuples.
    """
    ret = []
    for item in ip_list:
        ipaddr, port = item.split(":")
        port = int(port)
        ret.append(ipv4_2_ipv6((ipaddr, port)))
    return ret


def ipv4_2_ipv6(addr: tuple) -> tuple:
    """Prefixes IPv4 addresses so they can be used
    with an IPv6 socket

    Parameters
    ----------
    addr: tuple[str, int]
        A tuple in the form (IP, port).

    Returns
    -------
    tuple[str, int]
        Tuple containing an (IPv6, port) tuple.
    """
    assert len(addr) == 2, "addr should be formatted as (addr, port)"
    assert isinstance(addr, tuple), "addr should be a (addr, port) tuple"
    ipaddr, port = ip_address(addr[0]), addr[1]
    if ipaddr.version == 4:
        ipaddr = f"::ffff:{ipaddr}"
        return (ipaddr, port)
    return addr
