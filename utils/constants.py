from enum import Enum, IntFlag, auto
from http import HTTPStatus

from aioquic.quic.packet import QuicErrorCode

from handlers.ws import MessageController


class BHeaders(auto):
    AUTHORITY = b":authority"
    METHOD = b":method"
    PATH = b":path"
    PROTOCOL = b":protocol"
    SCHEME = b":scheme"
    STATUS = b":status"
    # Other useful headers
    SUPPORTED_WEBSOCKET_CHANNELS = b"supported-ws-channels"
    WEBSOCKET_CHANNEL = b"ws-channel"
    CHALLENGE = b"challenge"


class WSChannels(IntFlag):
    """Supported websocket channels

    All clients should at least support
    CTRL and MESG channels

    This field should ideally be an uint8 instance
    in other languages.
    """

    # Control channel
    CTRL_CHANNEL = 0b00000001
    # Message channel+
    MESG_CHANNEL = 0b00000010
    # Optional file channel
    FILE_CHANNEL = 0b00000100

    @classmethod
    def get_from_bytes(cls, value):
        """Dict-like behavior for IntFlag enum"""
        return cls._value2member_map_.get(int.from_bytes(value[:1], byteorder="little", signed=False), None)

    @classmethod
    def from_bytes(cls, value):
        """Create an instance of WSChannels from bytes."""
        return cls(int.from_bytes(value[:1], byteorder="little", signed=False))


class States(Enum):
    """Auth states

    AUTH_PENDING = client still doesn't send their pub x509 cert
    AUTH_SENT_CHALLENGE = received pub cert from client, server
    sent random bytes challenge
    AUTH_DONE = client may open websocket channels now
    """

    AUTH_PENDING = 0
    AUTH_SENT_CHALLENGE = 1
    AUTH_DONE = 2


class BHTTPStatus(auto):
    # 101
    SWITCHING_PROTOCOLS = f"{HTTPStatus.SWITCHING_PROTOCOLS}".encode()
    # 200
    OK = f"{HTTPStatus.OK}".encode()
    # 400
    BAD_REQUEST = f"{HTTPStatus.BAD_REQUEST}".encode()
    # 401
    UNAUTHORIZED = f"{HTTPStatus.UNAUTHORIZED}".encode()
    # 403
    FORBIDDEN = f"{HTTPStatus.FORBIDDEN}".encode()
    # 404
    NOT_FOUND = f"{HTTPStatus.NOT_FOUND}".encode()


REQUEST_BASE_HEADERS = {
    b":path": b"",
    b":method": b"",
    b":scheme": b"",
    b":authority": b"",
    b":protocol": b"",
}

WEBSOCKET_DEFAULT_HEADERS = {
    b"sec-websocket-version": b"13",
    b"sec-websocket-key": b"",
    b"connection": b"Upgrade",
    b"upgrade": b"websocket",
}

WEBSOCKET_DEFAULT_RESPONSE_HEADERS = {
    b"connection": b"Upgrade",
    b"upgrade": b"websocket",
    b"sec-websocket-accept": b"",
}

WEBSOCKET_CONTROLLER = {
    WSChannels.CTRL_CHANNEL: MessageController,
    WSChannels.MESG_CHANNEL: MessageController,
    WSChannels.FILE_CHANNEL: MessageController,
}

# Fixed UUID as per RFC
WS_HASH = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11".encode()


class QuicCloseReasons(auto):
    FAILED_CHALLENGE = {
        "error_code": QuicErrorCode.APPLICATION_ERROR,
        "reason_phrase": "challenge verification failed",
    }
    FAILED_CLIENTCERT = {
        "error_code": QuicErrorCode.APPLICATION_ERROR,
        "reason_phrase": "unable to load client x509 cert",
    }
    UNKNOWN_REQUEST = {
        "error_code": QuicErrorCode.APPLICATION_ERROR,
        "reason_phrase": "unsupported method/path",
    }
