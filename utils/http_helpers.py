import logging
from base64 import b64decode, b64encode
from datetime import datetime
from enum import auto
from hashlib import sha1
from random import randbytes
from secrets import token_bytes

from cryptography.exceptions import InvalidSignature
from cryptography.hazmat.primitives.asymmetric.ed25519 import Ed25519PublicKey
from cryptography.x509 import load_pem_x509_certificate

from .constants import (
    REQUEST_BASE_HEADERS,
    WEBSOCKET_DEFAULT_HEADERS,
    WEBSOCKET_DEFAULT_RESPONSE_HEADERS,
    WS_HASH,
    BHeaders,
    States,
)

log = logging.getLogger(__name__)


def simple_hash(data: bytes):
    """
    Calculate the SHA-1 hash of a bytearray
    """
    hasher = sha1()
    hasher.update(data)
    return hasher.digest()


class HeaderBuilder:
    def __init__(self):
        self._headers = {}

    @classmethod
    def request(cls, path: str, method: str):
        ret = cls()
        ret._headers |= REQUEST_BASE_HEADERS
        ret._headers[BHeaders.PATH] = path.encode()
        ret._headers[BHeaders.METHOD] = method.encode()
        return ret

    @classmethod
    def response(cls, status=401):
        ret = cls()
        ret._headers[b":status"] = f"{status}".encode()
        return ret

    @classmethod
    def websocket_response(cls, status=101, key=b""):
        ret = cls()
        ret._headers[b":status"] = f"{status}".encode()
        ret._headers |= WEBSOCKET_DEFAULT_RESPONSE_HEADERS
        ret._headers[b"sec-websocket-accept"] = ws_calculate_hash(key)
        return ret

    @classmethod
    def websocket(cls, path: str, key: bytes):
        ret = cls.request(path, "GET")
        ret._headers |= WEBSOCKET_DEFAULT_HEADERS
        ret._headers[b"sec-websocket-key"] = key
        return ret

    @classmethod
    def https(cls, path: str, method: str):
        ret = cls.request(path, method)
        ret._headers[BHeaders.PROTOCOL] = b"https"
        ret._headers[BHeaders.SCHEME] = b"https"
        return ret

    def add(self, key: bytes, value: bytes):
        self._headers[key] = value
        return self

    def build(self):
        return list(self._headers.items())


class HttpStatusResponses(auto):
    BAD_REQUEST = HeaderBuilder.response(400).build()


def validate_websocket_request(headers) -> bool:
    """Validates incoming websocket requests.

    The sec-websocket-key should be at least 32-bit in length.

    Parameters
    ----------
    headers: dict
        A dict object which should include at least all the
        keys included in WEBSOCKET_DEFAULT_HEADERS.

    Returns
    -------
    bool
        True if all the expected values are included in [headers],
        False otherwise.
    """
    if key := headers.get(b"sec-websocket-key", b""):
        if len(key) >= 32:
            for key, value in WEBSOCKET_DEFAULT_HEADERS.items():
                if key == b"sec-websocket-key":
                    # skip this key since it's going to be always different
                    continue
                if headers.get(key, b"") != value:
                    return False
            return True
    return False


def validate_websocket_response(headers: dict, expected_hash: bytes) -> bool:
    """Validates incoming websocket responses.

    Parameters
    ----------
    headers: dict
        A dict object, which ideally should be a response to a websocket request
        the client initiated.

    expected_hash: bytes
        Expected hash.

    Returns
    -------
    bool
        True both the expected values are included in [headers] and hash
        is indeed correct, False otherwise.
    """
    return (
        headers.get(b"sec-websocket-accept") == expected_hash
        and headers.get(b"connection") == b"Upgrade"
        and headers.get(b"upgrade") == b"websocket"
    )


def ws_calculate_hash(key: bytes) -> bytes:
    hasher = sha1()
    hasher.update(key)
    hasher.update(WS_HASH)
    return hasher.digest()


def ws_hashpair() -> tuple:
    key = randbytes(32)
    keyhash = ws_calculate_hash(key)
    return (key, keyhash)


class SimpleHTTPAuthenticator:
    def __init__(self, identifier, future, pre_incoming_connection):
        self._id = identifier
        self._future = future
        self.pre_incoming_connection = pre_incoming_connection
        self.register_nodeid = lambda client_nodeid: None
        self._client_cert = None
        self.client_cert_sign = None
        self.client_nodeid = None
        self._client_cert_bytes = None
        self._random = token_bytes(256)
        self.state = States.AUTH_PENDING

    def handle_get(self, headers) -> list:
        """Handles a GET petition.

        Notes
        -----
        The :authority pseudo-protocol should include
        a valid x509 public certificate.

        This pseudo-header is used because this app isn't geared
        towards centralized stuff. Since it's mandatory on pretty
        much every request, let's put it into use without
        wasting extra bytes on yet another custom header.
        """
        if self.state == States.AUTH_PENDING:
            if client_pubkey := headers.get(BHeaders.AUTHORITY, b""):
                try:
                    self._client_cert = load_pem_x509_certificate(client_pubkey)
                    if not isinstance(self._client_cert.public_key(), Ed25519PublicKey):
                        # Enforce usage of Ed25519PublicKey keys
                        log.error("type(remote_key) != Ed25519PublicKey")
                        return False
                    self._client_cert_bytes = client_pubkey
                    if self._client_cert.not_valid_after < datetime.utcnow() < self._client_cert.not_valid_before:
                        log.error("client cert is either expired or not valid (yet)")
                        return False
                except ValueError:
                    log.warning("load_pem_x509_certificate(): malformed key")
                    return False

                # Ensure the user actually wants this connection to be made
                if self.pre_incoming_connection(self._client_cert):

                    self.client_cert_sign = self._client_cert.signature
                    # NodeID is just the sha1 hash of client's pub x509 cert
                    self.client_nodeid = simple_hash(client_pubkey).hex()

                    self.state = States.AUTH_SENT_CHALLENGE
                    log.info("[%s] %s", self._id, self.state.name)
                    return HeaderBuilder.response(200).add(b"challenge", self._random)
        return False

    def validate_headers(self, headers) -> bool:
        """
        Validates headers for an incoming websocket request.

        Parameters
        ----------
        headers: dict
            Client's headers dict.
        """
        if self.state == States.AUTH_DONE:
            return True

        try:
            token = headers.get(b"token", b"")
            assert len(token) == 64, "Wrong token length"
            self._client_cert.public_key().verify(token, self._random)

            self.register_nodeid(self.client_nodeid)
            self.state = States.AUTH_DONE
            self._future.set_result(True)
            log.info("[%s] %s", self._id, self.state.name)
            return True
        except InvalidSignature:
            log.error("[%s] %s failed challenge", self._id, headers)

        except AssertionError as error:
            log.error("[%s] AssertionError: %s", self._id, error)

        return False
