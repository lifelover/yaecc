from gi.repository import Gtk as gtk


def simple_dialog(root, title, contents) -> None:
    """
    Displays a simple MessageDialog.

    Parameters
    ----------
    root: GtkWindow
        The root on which this dialog should be displayed.

    title: str
        MessaeDialog title.

    contents: str
        MessageDialog message.
    """
    dialog = gtk.MessageDialog(
        transient_for=root,
        flags=0,
        message_type=gtk.MessageType.INFO,
        buttons=gtk.ButtonsType.OK,
        text=title,
    )
    dialog.format_secondary_text(contents)
    dialog.run()
    dialog.destroy()


def confirm_dialog(root, title, contents) -> None:
    """
    Displays a simple MessageDialog.

    Parameters
    ----------
    root: GtkWindow
        The root on which this dialog should be displayed.

    title: str
        MessaeDialog title.

    contents: str
        MessageDialog message.
    """
    dialog = gtk.MessageDialog(
        transient_for=root,
        flags=0,
        message_type=gtk.MessageType.QUESTION,
        buttons=gtk.ButtonsType.OK_CANCEL,
        text=title,
    )
    dialog.format_secondary_text(contents)
    response = dialog.run()
    dialog.destroy()
    return response == gtk.ResponseType.OK
