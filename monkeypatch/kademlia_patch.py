"""
Kademlia monkey-patch tailored to Yaecc's needs
"""

import logging
from hashlib import sha1

from kademlia.node import Node
from kademlia.protocol import KademliaProtocol

log = logging.getLogger(__name__)


class PatchedKademliaProtocol(KademliaProtocol):
    """
    This class patches the original rpc_store method
    so that we don't store useless values (certificates)
    with a mismatched hash
    """

    def rpc_store(self, sender, nodeid, key, value):
        hasher = sha1()
        hasher.update(value)
        if key == hasher.digest():
            source = Node(nodeid, sender[0], sender[1])
            self.welcome_if_new(source)
            log.debug("store request from %s, storing key %s", sender, key.hex())
            self.storage[key] = value
            return True
        log.warning("hash mismatch (%s) for key %s", sender, key.hex())
        return False


def patch():
    """
    Monkey-patches the rpc_store method
    """
    KademliaProtocol.rpc_store = PatchedKademliaProtocol.rpc_store
