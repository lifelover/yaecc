"""
YAECC's CLI utility
You can use this to debug or to implement custom
stuff using the underlying classes and channels.

You can start using this utility first creating a Ed25519
TLS keypair. Here's how to do it with OpenSSL:

openssl req -new -newkey ed25519 -x509  -days 365 -nodes -out PubX509.crt -keyout Priv.key

After that, you can either run a (hybrid) server instance with:

python main.py --sk Server.key --pk Server.crt --port 8443 --bootstrap '192.168.0.16:8000'

Or alternatively, you can connect to someone else searching their IP address using their public X509 cert:

python http3_server.py --pk MyCertificate.crt --sk MyKey.key --port 8444  --bootstrap 'IP:PORT' --search-cert Server.crt

"""

import argparse
import asyncio
import logging
import socket
from hashlib import sha1

import aioconsole
from aioquic.h3.connection import H3_ALPN
from aioquic.quic.configuration import QuicConfiguration
from cryptography.x509 import load_pem_x509_certificate

from handlers.quicclient import QuicClientHandler
from handlers.quicserver import QuicServerHandler
from p2p.core import P2PQuicHandler
from p2p.user_callbacks import QUICCallbacks
from utils.constants import WSChannels
from utils.ip import nargs_ips

try:
    import uvloop
except ImportError:
    uvloop = None

kadlog = logging.getLogger("kademlia")
kadlog.setLevel(logging.DEBUG)
# kadlog.addHandler(logging.StreamHandler())

logging.basicConfig(level=logging.INFO, format="%(asctime)s [%(levelname)s] %(message)s")
log = logging.getLogger(__name__)


async def loss_timer(quic_connection):
    while True:
        await asyncio.sleep(1)
        print("_loss_at: ", quic_connection._loss_at)


async def amain():
    parser = argparse.ArgumentParser(description="YAECC Server/Client")
    parser.add_argument("--sk", type=str, required=True, help="private key")
    parser.add_argument("--pk", type=str, required=True, help="x509 server public key")
    parser.add_argument("--port", type=int, required=True, help="local serving & listening port")
    parser.add_argument("--bootstrap", nargs="+", required=False, default=[], help="bootstrap nodes")
    parser.add_argument("-s", "--search-cert", type=str, required=False)
    args = parser.parse_args()
    args.bootstrap = nargs_ips(args.bootstrap)

    secrets = open("secrets.log", "w")

    configuration = QuicConfiguration(
        H3_ALPN, is_client=False, secrets_log_file=secrets, idle_timeout=120, max_stream_data=1
    )
    # load SSL certificate and key
    configuration.load_cert_chain(args.pk, args.sk)

    # configuration.load_verify_locations("Expired.crt")
    callback_cls = QUICCallbacks()

    sock = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind(("::", args.port))
    _, proto_core = await loop.create_datagram_endpoint(
        lambda: P2PQuicHandler(
            configuration=configuration,
            callback_cls=callback_cls,
            create_protocol=QuicServerHandler,
            create_protocol_client=QuicClientHandler,
        ),
        sock=sock,
    )

    if args.bootstrap:
        await proto_core.bootstrap_async(args.bootstrap)
    else:
        log.info("No bootstrap node(s) provided - we are alone")

    if args.search_cert:
        # Load remote cert
        hasher = sha1()
        with open(args.search_cert, "rb") as handle:
            cert_contents = handle.read()
            hasher.update(cert_contents)
            sha1hash = hasher.digest().hex()
            cert = load_pem_x509_certificate(cert_contents)
        configuration.load_verify_locations(args.search_cert)
        # Attempt to find and connect
        if address := await proto_core.find_by_cert(cert_contents):
            print("found: ", address)
            if await proto_core.connect(address, cert.signature):
                print("connected with NodeID ", sha1hash)
                if conn := proto_core.by_nodeid.get(sha1hash, None):
                    # proto_core._loop.create_task(loss_timer(conn._quic))
                    while True:
                        if sock := conn.named_websockets.get(WSChannels.MESG_CHANNEL):
                            sock.send_data((await aioconsole.ainput(">> ")).encode())
                else:
                    print("No connection, exiting.")
            else:
                print(f"Address found at {address}, but we were unable to connect!")
        else:
            print("unable to find address on the DHT")

    else:
        while True:
            await aioconsole.ainput("Press enter to stick to the first connection available.")
            for conn in proto_core.by_nodeid.values():
                if sock := conn.named_websockets.get(WSChannels.MESG_CHANNEL):
                    while True:
                        sock.send_data((await aioconsole.ainput(f"ready [{conn._id}]>>>")).encode())


if __name__ == "__main__":

    if uvloop is not None:
        uvloop.install()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(amain())
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
