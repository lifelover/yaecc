"""
QuicConnectionProtocol but nicer.

This subclass of QuicConnectionProtocol implements some
useful functions on top of the original class in order to
make it easier working with websockets.
"""
import asyncio
import logging
import time
from weakref import WeakValueDictionary

from aioquic.asyncio import QuicConnectionProtocol

from utils.constants import WSChannels

log = logging.getLogger(__name__)


class QuicConnectionProtocolCB(QuicConnectionProtocol):
    """Base class built upon QuicConnectionProtocol with some extra
    methods.
    """

    def __init__(self, *args, **kwargs) -> None:
        self._callback_class = kwargs.pop("callback_class")
        super().__init__(*args, **kwargs)
        self._websockets = {}
        self._http = None
        self._id = None
        self._last_seen = None
        # Weak references to all websockets
        self._alive_task = None
        self.named_websockets = WeakValueDictionary()
        self._remote_cert = None

    def set_remote_cert(self, value):
        """
        Sets the remote certificate for the other party.
        """
        self._remote_cert = value

    @property
    def remote_cert(self):
        """
        Returns the remote certificate of the other party.
        """
        return self._remote_cert

    def clean_websockets(self):
        """
        Cleans opened websockets.
        """
        self._websockets.clear()

    def close(self, error_code=0x0, reason_phrase=""):
        # pylint: disable=arguments-differ
        """
        Brings the whole connection down.
        """
        self.clean_websockets()

        self._quic.close(error_code=error_code, reason_phrase=reason_phrase)
        self.transmit()

    def _remove_websocket(self, stream_id: int, channel: WSChannels) -> None:
        """
        Removes websocket handlers from self._websockets. These handlers
        are of no use anymore, and thus should be removed in order
        to prevent memory leaks.

        Parameters
        ----------
        stream_id: int
            Websocket Stream ID
        """

        # Nothing can work without this channel
        if channel == WSChannels.CTRL_CHANNEL:
            log.warning("[%s] cannot operate without CTRL_CHANNEL, closing QUIC connection!", self._id)
            self.close()
        del self._websockets[stream_id]
        log.info("[%s] closed websocket on stream_id=%s ", self._id, stream_id)

    def _send_headers(self, stream_id: int, headers: list, end_stream=False):
        self._http.send_headers(stream_id=stream_id, headers=headers, end_stream=end_stream)
        self.transmit()

    def _send_data(self, stream_id: int, data: bytes, end_stream=False):
        self._http.send_data(stream_id=stream_id, data=data, end_stream=end_stream)
        self.transmit()

    async def _keep_alive(self, every: int = 60) -> None:
        """Pings the other peer periodically in order to keep
        the NAT mapping alive

        Parameters
        ----------
        every: int
            Ping the remote node every [every] seconds.

        Notes
        -----
        Higher values than 30 seconds might actually cause the
        connection to be dropped with bad-behaved NATs. It might work,
        but 30 seconds is quite a safe, conservative value which will
        work with pretty much any NAT.

        By default, the server will ping the other node every 60
        seconds. Clients will ping the server every 30 or less seconds, since
        some NATs seem to be 'friendlier' with client-initiated traffic.
        """

        # Wait initially some time while channels are set up
        await asyncio.sleep(10)

        # Close connection if there are no active websockets
        # Control channel should be always up, otherwise
        # this connection is just pointless without active comm channels

        while self.named_websockets.get(WSChannels.CTRL_CHANNEL):
            await asyncio.sleep(every)
            if time.monotonic() - self._last_seen > every:
                try:
                    # Do not wait more than 10 sec for ping packet to arrive
                    await asyncio.wait_for(self.ping(), timeout=10)
                except (ConnectionError, asyncio.TimeoutError):
                    log.info("[%s] closing dead/useless connection", self._id)
                    break
        self.close()
