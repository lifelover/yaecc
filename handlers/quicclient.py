import argparse
import asyncio
import logging
import time
from binascii import hexlify
from hashlib import sha1
from random import randbytes
from secrets import token_bytes
from typing import Deque, Dict, Optional, cast

import aioquic
from aioquic.h3.connection import H3_ALPN, H3Connection
from aioquic.h3.events import DataReceived, H3Event, HeadersReceived
from aioquic.quic.events import ProtocolNegotiated, QuicEvent
from cryptography.hazmat.primitives.serialization import Encoding
from wsproto.events import BytesMessage, CloseConnection

from utils.constants import WEBSOCKET_CONTROLLER, BHeaders, BHTTPStatus, WSChannels
from utils.http_helpers import HeaderBuilder, simple_hash, validate_websocket_response, ws_hashpair

from .quicbase import QuicConnectionProtocolCB
from .ws import WSHandler

log = logging.getLogger(__name__)


class QuicClientHandler(QuicConnectionProtocolCB):
    """QUIC Connection handler

    Manages the underlying QUIC protocol and websockets which
    sit on top of QUIC.

    Attributes
    ----------
    _websockets: dict
        Dict containing handles for each opened websocket.

    _websockets_asgi: dict
        Dict containing handles for each ASGI instance on the websocket.

    _last_seen: int
        Last time this node was seen

    _id: str
        Hexadecimal ID for this connection

    _client: tuple
        Tuple containing the (ip, address) of this peer

    _alive_task: [asyncio Task]
        Asyncio task. Periodically pings the other node if they haven't
        sent us data in a while.
    """

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self._websockets = {}
        self._last_seen = time.monotonic()
        self._id = hexlify(self._quic.original_destination_connection_id).decode()
        self._http = None
        self._client = None
        self._alive_task = None
        # CLIENT ONLY
        self._challenge_signed = b""
        self._fut_challenge_complete = self._loop.create_future()
        self._fut_websocket_complete = self._loop.create_future()
        self._http_handler_queue = {}
        # Keep a reference to our pub cert to send it later to the server
        self._server_supported_channels = None
        # Our own pub and priv keys
        self._pkey_bytes = None
        self._skey = None
        # Remote's NodeID/Signature
        self._remote_cert = None
        self._remote_nodeid = None
        self._remote_cert_sign = None

    def set_keypairs(self, pkey_bytes: bytes, skey) -> None:
        """
        Sets client's keys on this class.

        These keys are needed so that we can authenticate against
        another (server) peer.
        """
        self._pkey_bytes = pkey_bytes
        self._skey = skey

    def init_remote_attrs(self) -> None:
        """
        Sets attributes from remote peer (server)
        """
        self._remote_cert = self._quic.tls._peer_certificate
        self._remote_cert_sign = self._remote_cert.signature
        self._remote_nodeid = simple_hash(self._remote_cert.public_bytes(Encoding.PEM)).hex()

    @property
    def is_incoming(self):
        return False

    @property
    def nodeid(self):
        """
        Returns remote's NodeID.
        """
        return self._remote_nodeid

    @property
    def cert_signature(self):
        """
        Returns remote's certificate signature.
        """
        return self._remote_cert_sign

    @classmethod
    def as_client(cls, *args, **kwargs):
        """Creates this class in client-mode.

        It is necessary to override the internal attribute
        QuicConnectionProtocol._is_client in order to bypass
        aioquic's limitations.
        """
        ret = cls(*args, **kwargs)
        ret._id = hexlify(ret._quic.host_cid).decode()
        return ret

    async def _client_handle_websocketresponse(self, _event, headers):
        """Websocket response handler

        Parameters
        ----------
        _event: H3Event
            HTTP/3 event.

        headers; dict
            Response headers.

        Notes
        -----

        """
        if headers[BHeaders.STATUS] == BHTTPStatus.SWITCHING_PROTOCOLS:
            self._fut_websocket_complete.set_result(headers)
        else:
            self._fut_websocket_complete.set_result(False)

    async def _client_handle_challenge(self, _event, headers):
        """Challenge handler

        This function is meant to be called once a challenge has been received
        from the server.
        """
        if headers[BHeaders.STATUS] == BHTTPStatus.OK:
            if supported_channels := headers.get(BHeaders.SUPPORTED_WEBSOCKET_CHANNELS, b""):
                # extract 1-byte unsigned int
                supported_channels = WSChannels.from_bytes(supported_channels)
                self._server_supported_channels = set(
                    channel for channel in WSChannels if channel in supported_channels
                )
                assert WSChannels.CTRL_CHANNEL in supported_channels, "Server should at least support CTRL_CHANNEL"
                log.info("[%s] server channel support: %s", self._id, supported_channels)

                if challenge := headers.get(BHeaders.CHALLENGE):
                    log.info("[%s] signing %s-byte challenge", self._id, len(challenge))
                    self._challenge_signed = self._skey.sign(data=challenge)
                    self._fut_challenge_complete.set_result(True)
                    return

        log.error("[%s] S:%s / server didn't send challenge", self._id, headers[BHeaders.STATUS])
        self._fut_challenge_complete.set_result(False)

    def http_event_received(self, event: H3Event) -> None:
        """
        Simple async wrapper for this event
        """
        self._loop.create_task(self.asynchttp_event_received(event))

    async def asynchttp_event_received(self, event: H3Event) -> None:
        """Handles HTTP events.

        Parameters
        ----------
        event: H3Event
            HTTP/3 event.


        Notes
        -----
        The flow pretty much goes like this if the client sent us a legitimate
        x509 public key in the headers

        [CLIENT]                        [SERVER]
        |---async connect()------------------->|

        -------------QUIC/CONNECTED------------ accept(), State.AUTH_PENDING

        |
        |do_client_auth()
        |---POST /auth------------------------>|
        |<------------200, challenge: blabla---| RESPONSE, State.AUTH_SENT_CHALLENGE
        |-websocket(), challenge-response ---->| peer_public_cert.verify(challenge_response)
        |<------------------------------ 200---| websocket is now open

        --------------------------------------- State.AUTH_DONE
        """

        # Pass HTTP events to an already established websocket
        if (wshandler := self._websockets.get(event.stream_id)) and isinstance(event, DataReceived):
            wshandler.http_event_received(event)

        elif isinstance(event, HeadersReceived):
            headers = dict(event.headers)

            if task := self._http_handler_queue.pop(event.stream_id, None):
                await task(event, headers)
            else:
                log.error("response for unknown stream: %s", event.stream_id)

    def quic_event_received(self, event: QuicEvent) -> None:
        """QUIC event handler"""
        self._last_seen = time.monotonic()

        if isinstance(event, ProtocolNegotiated):
            if event.alpn_protocol in H3_ALPN:
                self._http = H3Connection(self._quic)
                client_addr = self._http._quic._network_paths[0].addr
                self._client = (client_addr[0], client_addr[1])
                # Manually override internal parameter in order to allow
                # opening websockets while we are in server-mode
                self._http._is_client = True
                # Authenticate ourselves
                self._loop.create_task(self._do_client_auth())
                if not self._alive_task:
                    self._alive_task = asyncio.create_task(self._keep_alive(every=20))
            else:
                log.warning("QUIC: Unsupported ALPN protocol: %s", event.alpn_protocol)
                self.close()
                return

        # handle pending events, including connect/data packets
        if self._http:
            for http_event in self._http.handle_event(event):
                self.http_event_received(http_event)

    async def _do_client_auth(self) -> None:
        """Client-mode only

        Authenticates a client instance against the server. We'll
        asynchronously wait until the server sends a valid test token which us,
        the client must sign with own their private key.
        """
        stream_id = self._quic.get_next_available_stream_id()
        self._fut_challenge_complete = self._loop.create_future()
        self._http.send_headers(
            stream_id=stream_id,
            headers=HeaderBuilder.https(path="auth", method="GET").add(b":authority", self._pkey_bytes).build(),
        )
        self._http_handler_queue[stream_id] = self._client_handle_challenge

    async def _connect_channel(self, channel: WSChannels) -> None:
        """
        Open a WebSocket.
        """
        self._fut_websocket_complete = self._loop.create_future()

        # use another stream_id for this channel
        stream_id = self._quic.get_next_available_stream_id()
        key, expected_hash = ws_hashpair()
        self._send_headers(
            stream_id=stream_id,
            headers=HeaderBuilder.websocket(path="ws", key=key)
            .add(b"token", self._challenge_signed)
            .add(BHeaders.WEBSOCKET_CHANNEL, channel.to_bytes(length=1, byteorder="little"))
            .build(),
        )
        # Add callback for this stream id
        self._http_handler_queue[stream_id] = self._client_handle_websocketresponse
        await self._fut_websocket_complete
        # future's result should be True if everything went smoothly
        if headers := self._fut_websocket_complete.result():
            if validate_websocket_response(headers, expected_hash):
                handler = WEBSOCKET_CONTROLLER[channel].outwebsocket(
                    stream_id=stream_id,
                    channel=channel,
                    parent_cls=self,
                )
                self._websockets[stream_id] = handler
                self.named_websockets[channel] = self._websockets[stream_id]
                log.info("[%s] %s: ready", self._id, channel)
                return True

            log.error("[%s] malformed response, closing", self._id)
            self.close()
            return False

        log.error("[%s] unable to open websocket for channel %s", self._id, channel)
        return False

    async def try_setup(self):
        """
        Tries to set up communication channels, such as
        CTRL_CHANNEL, TEXT_CHANNEL, FILE_CHANNEL
        """

        # Wait for challenge response
        await self._fut_challenge_complete

        # Open all supported channels by server
        for channel in self._server_supported_channels:
            if not (await self._connect_channel(channel)):
                return False

        self._callback_class.connection_established(self)
        return True
