"""
Websocket handler stuff

"""
import logging
from weakref import WeakMethod, proxy

import wsproto
from wsproto.events import BytesMessage, CloseConnection

log = logging.getLogger(__name__)


class WSHandler:
    """Base WebSocket handler.

    This class isn't bound to any method or callback
    from the parent.
    """

    def __init__(self, *, stream_id: int, channel, parent_cls):
        """Constructor.

        Parameters
        ----------
        stream_id: int
            QUIC stream id for this websocket.

        channel: int[enum]
            Channel type for this websocket.

        parent_cls: Object
            Full-blown reference to the parent class that created
            this websocket. This is needed so that we can call
            methods from the parent (mostly callbacks).

        """
        # self.http_event_queue = deque()
        self.stream_id = stream_id
        self.channel = channel
        # Avoid keeping alive parent
        self.websocket = wsproto.Connection(wsproto.ConnectionType.SERVER)
        self._parent_cls = proxy(parent_cls)
        self.send_data_cb = WeakMethod(parent_cls._send_data)
        self.removecb = WeakMethod(parent_cls._remove_websocket)
        self._on_incoming_message = parent_cls._callback_class.incoming_message
        self._ws_buffer = bytearray()
        self._max_buff_len = 0  # bytes
        self._closed = False

    @classmethod
    def outwebsocket(cls, *args, **kwargs):
        ret = cls(*args, **kwargs)
        ret.websocket = wsproto.Connection(wsproto.ConnectionType.CLIENT)
        return ret

    def on_message(self, data) -> None:
        pass

    def on_disconnect(self) -> None:
        pass

    def http_event_received(self, event) -> None:
        """Process HTTP3 events on websocket"""
        self.websocket.receive_data(event.data)

        for ws_event in self.websocket.events():
            self.ws_event_recv(ws_event)

    def ws_event_recv(self, event: wsproto.events.Event) -> None:
        """
        Process websocket events.

        Notes
        -----
        Since WSHandler is only the base class for any websocket,
        using this class as-is will cause all streams
        to be immediately closed due to the 0-byte buffer capacity.
        """
        if isinstance(event, CloseConnection):
            self.on_disconnect()
            self._ws_buffer.clear()
            self.removecb(self.stream_id, self.channel)
            return

        # Allowing unlimited-length buffers could lead to DoS
        if not len(self._ws_buffer) > self._max_buff_len:
            self._ws_buffer += event.data
        else:
            # Abruptly close this stream if remote peer misbehaves
            self.send_data_cb()(self.stream_id, b"", end_stream=True)
            self._closed = True
            self.close(code=1009)  # CLOSE_TOO_LARGE
            log.error("Max buffer length (%s) exceeded - closing channel", self._max_buff_len)

        if event.message_finished:
            self.on_message(self._ws_buffer)
            self._ws_buffer.clear()

    def send_data(self, data: bytes):
        if data:
            self.send_data_cb()(
                stream_id=self.stream_id, data=self.websocket.send(BytesMessage(data=data)), end_stream=False
            )

    def close(self, code=1000):
        if not self._closed:
            self.send_data_cb()(
                stream_id=self.stream_id,
                data=self.websocket.send(CloseConnection(code=code)),
                end_stream=True,
            )
        self.removecb()(stream_id=self.stream_id, channel=self.channel)


class LimitedBuffWebsocketController(WSHandler):
    """
    Websocket handler with a small (1000-byte) buffer.

    Useful for text messages or JSON-based calls.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._max_buff_len = 1000


class MessageController(LimitedBuffWebsocketController):
    """
    Message controller.
    """

    def on_message(self, data: bytes):
        """
        On-incoming message callback.

        Messages must be UTF-8 formatted for .decode() to work.
        """
        self._on_incoming_message(self._parent_cls, data.decode())
