"""
Handler for server-sided connections.
"""
import asyncio
import logging
import time
from binascii import hexlify

from aioquic.h3.connection import H3_ALPN, H3Connection
from aioquic.h3.events import DataReceived, H3Event, HeadersReceived
from aioquic.quic.events import ProtocolNegotiated, QuicEvent

from utils.constants import WEBSOCKET_CONTROLLER, BHeaders, QuicCloseReasons, WSChannels
from utils.http_helpers import HeaderBuilder, HttpStatusResponses, SimpleHTTPAuthenticator, validate_websocket_request

from .quicbase import QuicConnectionProtocolCB
from .ws import WSHandler

log = logging.getLogger(__name__)


class QuicServerHandler(QuicConnectionProtocolCB):
    """QUIC Connection handler

    Manages the underlying QUIC protocol and websockets which
    sit on top of QUIC.

    Attributes
    ----------
    _websockets: dict
        Dict containing handles for each opened websocket.

    _websockets_asgi: dict
        Dict containing handles for each ASGI instance on the websocket.

    _last_seen: int
        Last time this node was seen

    _id: str
        Hexadecimal ID for this connection

    _client: tuple
        Tuple containing the (ip, address) of this peer

    _alive_task: [asyncio Task]
        Asyncio task. Periodically pings the other node if they haven't
        sent us data in a while.
    """

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self._last_seen = time.monotonic()
        self._id = hexlify(self._quic.original_destination_connection_id).decode()
        self._http = None
        self._client = None
        self._supported_channels = WSChannels.CTRL_CHANNEL | WSChannels.MESG_CHANNEL | WSChannels.FILE_CHANNEL
        self._connected_future = self._loop.create_future()
        self._auth = SimpleHTTPAuthenticator(
            self._id,
            future=self._connected_future,
            # The authenticator will trigger the pre_incoming_connection
            # so that the end user is able to reject this connection attempt
            # by checking their public certificate
            pre_incoming_connection=self._callback_class.pre_incoming_connection,
        )
        self._loop.create_task(self._on_connected_trigger())

    @property
    def is_incoming(self):
        return True

    @property
    def nodeid(self):
        """
        Returns remote's NodeID.
        """
        return self._auth.client_nodeid

    @property
    def cert_signature(self):
        """
        Returns remote's certificate signature.
        """
        return self._auth.client_cert_sign

    async def _on_connected_trigger(self):
        """
        This function is only called once, after the client has authenticated successfully.

        The receiving connection endpoint will wait at most 10 seconds for the client
        to authenticate themselves. If the client takes more than that, the connection
        will be automatically closed because who in hell takes more than that to authenticate.
        """
        try:
            await asyncio.wait_for(self._connected_future, timeout=10)
            self._callback_class.connection_established(self)
        except asyncio.TimeoutError:
            self.close()

    def __str__(self):
        return f"<QuicConnectionProtocol: {self._id}, channels={list(self.named_websockets)}, state={self._auth.state}>"

    def _server_handle_websocket(self, event: H3Event, headers: dict) -> list:
        """Authenticates remote peers attempting to connect to us"""

        # Pull channel from WEBSOCKET CHANNEL headerlist
        if channel := headers.get(BHeaders.WEBSOCKET_CHANNEL, b""):
            # Check if requested channel is valid
            if channel := WSChannels.get_from_bytes(channel):
                if channel not in self.named_websockets and channel in self._supported_channels:
                    # this channel is already open or we don't have support for it yet
                    if self._auth.validate_headers(headers) and validate_websocket_request(headers):
                        self._websockets[event.stream_id] = WEBSOCKET_CONTROLLER[channel](
                            stream_id=event.stream_id,
                            channel=channel,
                            parent_cls=self,
                        )

                        # Hold a weak reference to this handle as well with friendlier name
                        # i.e. named_websockets[WSChannels.FILE_CHANNEL] instead of
                        # an arbitrary and ever increasing event-id
                        self.named_websockets[channel] = self._websockets[event.stream_id]
                        log.info("[%s] %s: ready", self._id, channel)
                        # Switch protocol to websocket
                        return (
                            HeaderBuilder.websocket_response(key=headers.get(b"sec-websocket-key", b"")).build(),
                            False,
                        )

        log.warning("[%s] 400 - bad request on stream_id=%s", self._id, event.stream_id)
        return HttpStatusResponses.BAD_REQUEST, True

    def http_event_received(self, event: H3Event) -> None:
        """Handles HTTP events.

        Parameters
        ----------
        event: H3Event
            HTTP/3 event.


        Notes
        -----
        The flow pretty much goes like this if the client sent us a legitimate
        x509 public key in the headers

        [CLIENT]                        [SERVER]
        |---async connect()------------------->|

        -------------QUIC/CONNECTED------------ accept(), State.AUTH_PENDING

        |
        |do_client_auth()
        |---POST /auth------------------------>|
        |<------------200, challenge: blabla---| RESPONSE, State.AUTH_SENT_CHALLENGE
        |-websocket(), challenge-response ---->| peer_public_cert.verify(challenge_response)
        |<------------------------------ 200---| websocket is now open

        --------------------------------------- State.AUTH_DONE
        """

        # Pass HTTP events to an already established websocket

        if (wshandler := self._websockets.get(event.stream_id)) and isinstance(event, DataReceived):
            wshandler.http_event_received(event)

        elif isinstance(event, HeadersReceived):
            headers = dict(event.headers)
            # Headers were received for a websocket we initiated
            if headers.get(BHeaders.METHOD) == b"GET":
                path = headers.get(BHeaders.PATH, b"")
                if path == b"auth":
                    # First request - client sends cert, we then send token and supported channels
                    if headers := self._auth.handle_get(headers):
                        self._send_headers(
                            stream_id=event.stream_id,
                            headers=headers.add(
                                BHeaders.SUPPORTED_WEBSOCKET_CHANNELS,
                                self._supported_channels.to_bytes(length=1, byteorder="little"),
                            ).build(),
                            end_stream=True,
                        )
                    else:
                        self.close(**QuicCloseReasons.FAILED_CLIENTCERT)
                elif path == b"ws":
                    # Websocket handler
                    self._send_headers(event.stream_id, *self._server_handle_websocket(event, headers))
                else:
                    # unknown path
                    self.close(**QuicCloseReasons.UNKNOWN_REQUEST)
            else:
                # unknown method
                self.close(**QuicCloseReasons.UNKNOWN_REQUEST)

    def quic_event_received(self, event: QuicEvent) -> None:
        """Handle QUIC event"""
        self._last_seen = time.monotonic()

        if isinstance(event, ProtocolNegotiated):
            if event.alpn_protocol in H3_ALPN:
                self._http = H3Connection(self._quic)
                client_addr = self._http._quic._network_paths[0].addr
                self._client = (client_addr[0], client_addr[1])

                # Schedule keep-alive task
                if self._alive_task is None:
                    self._alive_task = asyncio.create_task(self._keep_alive())
            else:
                log.warning("Unsupported ALPN protocol: %s, expected HTTP/3", event.alpn_protocol)
                self.close()

        # handle pending events, including connect/data packets
        if self._http:
            for http_event in self._http.handle_event(event):
                self.http_event_received(http_event)
