# YAECC.

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)


YAECC is a P2P messaging tool - it allows you to exchange messages (text and files) in a secure, descentralized fashion, without any third parties involved analyzing or storing your data. 

## Is this a Tox clone?
Simple answer: Nope.

Long answer: While YAECC does pretty much all the stuff Tox does (mostly _secure_ data exchange), it's done in a completely different way, using state-of-the-art technologies. 

Unlike Tox, YAECC uses proved, well-established and mainstream cryptographic protocols - namely TLS 1.3 for everything that needs to be sent securely to other peers, along with a bleeding-edge transport layer (QUIC). 

Given this software is pretty much brand-new and has been planned and developed for roughly less than a year, there are way less features than those found in Tox: there are no video calls or any other fancy stuff. This software is pretty  much in an alpha state: bugs are to be expected, but hey, this is only the start of something good.

## How does it work?

Brief summary of techonologies and protocols used in this project:

| Protocol/Algorithm | Role in YAECC | Provided by | 
| ------ | ------ | ------ |
| Python 3.9+ | Programming language | - |
| TLS (1.3) | PKI protocol. X509 self-signed certificates are used by nodes to find other nodes in the DHT. | aioquic |
| QUIC | UDP-only secure transport | aioquic |
| Ed25519 | Digital Signature algorithm. YAECC deliberately enforces the usage of Ed25519 for pretty much everything - it actively verifies that both connecting sides use Ed25519, thus disallowing the usage of other algorithms, such as RSA or secp256r1 for Digital Signatures.| cryptography |
| Kademlia | Distributed Hash Table | kademlia |
| UI interface | GTK3+ is used to provide a graphical interface. However, the interal API can be easily decoupled from the GUI frontend since pretty much everything is handled by callbacks and accessors. A CLI-based utility is provided. | python-gobject |
| Websocket | Communication protocol. | aioquic |

This software relies on some black-magick hacks being applied to aioquic, the QUIC+TLS+HTTP3 library. Since aioquic is pretty much geared towards a client-server architecture, some changes had to be made in order to allow _hybrid_ nodes, this is, nodes that can both start and receive connections. 

## How to use it?

You can generate a new TLS keypair using ```openssl req -new -newkey ed25519 -x509 -days 365 -nodes -out public.crt -keyout private.key```.
You can literally enter-enter-enter all the fields that OpenSSL may ask you: none of the X509 fields (name, email, etc...) are used just for the sake of anonimity.

Below you'll find the possible args you can pass to the UI client (main.py).
| Flag | Description |
| --- | --- |
| ```--sk``` | Private key. |
| ```--pk``` | Public X509 key. |
| ```--port``` | Listening port. This port is going to be used for everything, including DHT and QUIC traffic. |
| ```--certdir``` | Directory where peer certificates are going to be stored and read from. OpenSSL will load these certificates when connecting to remote peers.|
| ```--contactdb``` | Contact database filename/location: this file holds usernames, certificates and other important stuff needed to communicate with other peers. |
| ```--bootstrap``` | List of bootstrap nodes. These are used as entry nodes, otherwise you won't be able to join the network.|

Example command:

` python main.py --sk private.key --pk cert.crt --port 59999 --bootstrap IP:PORT --certdir peercerts --contactdb db.pkl`

## Security considerations

There are some points to seriously keep in mind:

- This hasn't been peer-tested. It's designed and developed to be as secure as it can get, but one individual can only do so much. You've been warned.
- Just like Tox, YAECC doesn't actively try to hide your identity online. This is an active area of research: probably Dovetail could be used for everything that has to be routed over the DHT, but for the time being, it's only good ol' plain Kademlia.
- There are no tests! Most of the code has been carefully designed, though. Help is welcomed.
- This software is still in an early, alpha stage. Bugs are to be expected.

### Do I have to keep my private key under escrow?
Yes. Even though TLSv1.3 provides forward secrecy, if a given adversary gained access to your private key before a connection was established, they would be able to decrypt pretty much all of your traffic. This holds true for any situation where a private key has been leaked.

### Is it safe to exchange  data using self-signed TLS keys?
Yes! Before contacting any node, TLS public certs get added to your certificate store: in fact, this is how the internet works, operating system vendors bundle with your OS a fixed set of root CAs that are automatically trusted by default. YAECC takes a slightly different approach: you **EXPLICITELY** add your contacts through their public X509 certificate. We don't trust in root CAs, they are centralized and that somehow sucks, because if they were to be hacked or exploited that would mean that your OS would trust any certificate signed by them. You don't want that, do you?


## License

Copyright (C) 2021-forever and ever, Juan Manuel Dávila Méndez.

YAECC is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.


