import argparse
import asyncio
import logging
import signal
import socket
import threading
import time
from datetime import datetime
from functools import partial
from typing import Callable, Coroutine, Dict

import gi
from aioquic.h3.connection import H3_ALPN
from aioquic.quic.configuration import QuicConfiguration

from handlers.quicclient import QuicClientHandler
from handlers.quicserver import QuicServerHandler
from p2p.core import P2PQuicHandler
from p2p.user_callbacks import QUICCallbacks
from storage.db import DBInit, Contact, Conversation, DHTNode
from utils.constants import WSChannels
from utils.ip import nargs_ips, ipv4_2_ipv6

gi.require_version("Gtk", "3.0")
gi.require_version("Notify", "0.7")

from gi.repository import GLib, Gtk, Notify

from utils.dialogs import confirm_dialog, simple_dialog

logging.basicConfig(level=logging.INFO, format="%(asctime)s [%(levelname)s] %(message)s")
log = logging.getLogger(__name__)


def hide_on_delete(widget, _event):
    widget.hide()
    return True


def simple_notify(title, contents) -> None:
    """
    Display a simple notification.
    """
    Notify.Notification.new(title, contents, "dialog-information").show()


class Callbacks(QUICCallbacks):
    def __init__(
        self,
        pre_incoming_connection,
        incoming_message,
        connection_established,
        connection_closed,
    ):
        self.pre_incoming_connection = pre_incoming_connection
        self.incoming_message = incoming_message
        self.connection_established = connection_established
        self.connection_closed = connection_closed


class MainWindow:
    def __init__(self, args):
        ui_file = "main.glade"
        self._args = args
        self.builder = Gtk.Builder()
        self.builder.add_from_file(ui_file)
        DBInit(self._args.contactdb, self._args.certdir)
        Notify.init("YAECC")
        # self._set_sizes()
        # Init internal stuff
        self._current_selected_nodeid = None
        # Create and bind UDP socket
        self._socket = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
        self._socket.bind(("::", args.port))
        self._loop = asyncio.get_event_loop()

        # Launch internal QUIC+DHT servers
        self._protocol = None
        self._quic_config = None
        self._thread_loop = threading.Thread(target=self._launch_server_threaded)
        self._thread_loop.start()
        self._tag_table = Gtk.TextTagTable()
        self._tag_table.add(Gtk.TextTag(name="right", justification=Gtk.Justification.RIGHT))
        # self._gtk_text_buffers: Dict[str, Gtk.TextBuffer] = {}
        self._gtk_text_buffers: Dict[str, Gtk.TextBuffer] = {
            contact.nodeid: Gtk.TextBuffer.new(self._tag_table) for contact in Contact.select()
        }
        self._bind_objects()
        self._update_contacts_view()
        self._init_dhtpeers_view()
        self._populate_txbuffers_from_database()
        self.window.show()

    def _populate_txbuffers_from_database(self) -> None:
        """
        Populates Gtk TextBuffer(s) with data stored in the local database.
        """
        for contact in Contact.select():
            for db_msg in Conversation.select().where(Conversation.contact == contact):
                if db_msg.is_local:
                    txbuff_insert = f"{db_msg.message}\t[You/{db_msg.date.strftime('%d %H:%M:%S')}]"
                else:
                    txbuff_insert = f"[{contact.username}/{db_msg.date.strftime('%d %H:%M:%S')}] {db_msg.message}"
                self._insert_text_at_buffer(
                    buffer=contact.nodeid,
                    text=txbuff_insert,
                    is_local=db_msg.is_local,
                )

    def _bind_objects(self) -> None:
        """
        Binds UI callbacks to python functions.

        FIXME: Use glade actions to bind actions
        """
        self.window = self.builder.get_object("MainWindow")
        self.window.connect("delete-event", Gtk.main_quit)
        self.window.connect("delete-event", self.die)
        self.about_window = self.builder.get_object("about_window")

        popover_menu = self.builder.get_object("popover_menu")
        popover_menu.set_position(Gtk.PositionType.BOTTOM)

        popover_menu_box = self.builder.get_object("popover_menu_box")
        popover_menu.add(popover_menu_box)

        popover_button_about = self.builder.get_object("popover_button_about")
        popover_button_about.connect("pressed", self._on_about_press)

        popover_button_add_contact = self.builder.get_object("popover_button_add_contact")
        dht_peers_add_new_node_button = self.builder.get_object("dht_peers_add_new_node")
        # popover_button_add_contact.connect("pressed", self._on_add_contact)

        # notifications_switch = self.builder.get_object("notifications_switch")
        # notifications_switch.connect("state-set", self._on_notifications_switch)

        self.main_pane_contacts = self.builder.get_object("main_pane_contacts")
        self._main_chat_contact_label = self.builder.get_object("main_chat_contact_label")
        self.add_contact_username_entry = self.builder.get_object("add_contact_username_entry")
        self.add_new_peer_address = self.builder.get_object("add_new_peer_address")
        self.add_new_peer_port = self.builder.get_object("add_new_peer_port")
        self.notifications_switch = self.builder.get_object("notifications_switch")

        self.header_bar = self.builder.get_object("header_bar")

        # windows
        self.add_contact_window = self.builder.get_object("AddContactWindow")
        self.add_contact_window.connect("delete-event", hide_on_delete)

        self.add_node_window = self.builder.get_object("AddNodeWIndow")
        self.add_node_window.connect("delete-event", hide_on_delete)

        popover_button_add_contact.connect("pressed", lambda _button: self.add_contact_window.show())
        dht_peers_add_new_node_button.connect("pressed", lambda _button: self.add_node_window.show())
        # Treeviews
        self.contacts_treeview = self.builder.get_object("contacts_treeview")
        self.dht_peers_treeview = self.builder.get_object("dht_peers_treeview")
        self.main_text_view = self.builder.get_object("main_text_view")

        self._dummy_txt_buffer = Gtk.TextBuffer()
        self.main_text_view.set_buffer(self._dummy_txt_buffer)

        tree_contacts_selection = self.builder.get_object("tree_contacts_selection")
        tree_contacts_selection.connect("changed", self.on_contact_row_change)

        contacts_connect_button = self.builder.get_object("contacts_connect_button")
        contacts_connect_button.connect("pressed", self._on_connect_button_press)

        dht_peers_delete_button = self.builder.get_object("dht_peers_delete_button")
        dht_peers_delete_button.connect("pressed", self._dhtnode_remove_selected)

        contacts_remove_selected = self.builder.get_object("contacts_remove_selected")
        contacts_remove_selected.connect("pressed", self._contacts_remove_selected)

        self._main_textentry = self.builder.get_object("main_textentry")
        self._main_textentry.connect("activate", self._on_main_textentry)
        self._main_textentry.set_editable(False)

        button_send_message = self.builder.get_object("button_send_message")
        button_send_message.connect("pressed", self._on_main_textentry)

        button_add_new_contact = self.builder.get_object("button_add_new_contact")
        button_add_new_contact.connect("pressed", self._on_add_new_contact)

        button_add_new_node = self.builder.get_object("button_add_new_node")
        button_add_new_node.connect("pressed", self._on_add_new_node)

        button_dht_peers_refresh = self.builder.get_object("dht_peers_refresh")
        button_dht_peers_refresh.connect("pressed", self._update_dhtpeers_view)

        self.contact_filechooser = self.builder.get_object("contact_filechooser")

        main_notebook = self.builder.get_object("main_notebook")
        main_notebook.connect("switch-page", self._on_page_switch)

        self._contact_store = self.builder.get_object("contacts_store")
        self._dhtpeers_store = self.builder.get_object("dhtpeers_store")

        self.main_chat_input_pane = self.builder.get_object("main_chat_input_pane")
        self.main_chat_input_pane.set_position(1000)

    def _launch_server_threaded(self) -> None:
        """
        Launch event loop in separate light thread so that
        asyncio's event loop doesn't get blocked by GLib's
        loop
        """
        self._loop.run_until_complete(self._setup_server())
        self._loop.run_forever()

    async def _setup_server(self) -> None:
        self._quic_config = QuicConfiguration(H3_ALPN, is_client=False, idle_timeout=120)
        # load both public and secret key
        self._quic_config.load_cert_chain(self._args.pk, self._args.sk)
        callback_cls = Callbacks(
            pre_incoming_connection=self._pre_incoming_connection,
            incoming_message=self._incoming_message,
            connection_established=self._connection_established,
            connection_closed=self._connection_closed,
        )
        _, self._protocol = await self._loop.create_datagram_endpoint(
            lambda: P2PQuicHandler(
                configuration=self._quic_config,
                callback_cls=callback_cls,
                create_protocol=QuicServerHandler,
                create_protocol_client=QuicClientHandler,
            ),
            sock=self._socket,
        )

        log.info("DHT: attempting bootstrap")
        count = await self._protocol.bootstrap_async(DHTNode.get_as_tuples())
        if not count:
            # Thread-safe call
            GLib.idle_add(
                simple_dialog,
                self.window,
                "Couldn't bootstrap ourselves into the DHT!",
                "You won't be able to find peers or be contacted. Try adding some bootstrapping nodes.",
            )
        self._change_headerbar_subtitle(f"DHT: {count} peers")

    async def _retrigger_bootstrapping(self):
        await self._protocol.bootstrap_async(DHTNode.get_as_tuples())
        dht_peers = self._protocol.dht.bootstrappable_neighbors()
        self._change_headerbar_subtitle(f"DHT: {len(dht_peers)} peers")

    def _change_headerbar_subtitle(self, message) -> None:
        GLib.idle_add(self.header_bar.set_subtitle, message)

    def _gtk_text_buffer_scroll(self, buffer) -> None:
        self.main_text_view.scroll_to_mark(
            buffer.get_insert(),
            0.0,  # within_margin
            True,  # use_align
            0.5,  # xalign
            0.5,  # yalign
        )

    def _insert_text_at_buffer(self, buffer: Gtk.TextBuffer, text: str, is_local: str = False) -> None:
        """

        Parameters
        ----------
        buffer: Gtk.TextBuffer
            Text buffer for a given remote.

        text: str
            The string to insert.

        is_local: bool
            If True, insert text with Gtk.Justification.RIGHT.
            Left justification otherwise.
        """
        if is_local:
            # is_local = us, basically use right justification
            self._gtk_text_buffers[buffer].insert_with_tags_by_name(
                self._gtk_text_buffers[buffer].get_end_iter(), text + "\n", "right"
            )
        else:
            self._gtk_text_buffers[buffer].insert_at_cursor(text + "\n")

        # Scroll down if we're seeing this buffer, otherwise just append text to buffer
        if self.main_text_view.get_buffer() == self._gtk_text_buffers[buffer]:
            self._gtk_text_buffer_scroll(self._gtk_text_buffers[buffer])

    def _get_current_selected_nodeid(self, tree_selection=None) -> str:
        """
        Returns the currently selected NodeID for the main contact panel
        (the one in the first tab).

        Parameters
        ----------
        """
        if tree_selection:
            model, iterator = tree_selection.get_selected()
        else:
            model, iterator = self.main_pane_contacts.get_selection().get_selected()

        if iterator:
            return model[iterator][1]  # 1 for nodeid attribute
        return None

    def on_contact_row_change(self, tree_selection) -> None:
        """
        Changes the text view accordingly.
        """
        self._current_selected_nodeid = self._get_current_selected_nodeid(tree_selection)

        if self._current_selected_nodeid:
            gtk_txt_buffer = self._gtk_text_buffers[self._current_selected_nodeid]
            self.main_text_view.set_buffer(gtk_txt_buffer)
            self._gtk_text_buffer_scroll(gtk_txt_buffer)
            self._main_textentry.set_editable(True)
            if self._protocol.by_nodeid.get(self._current_selected_nodeid, None):
                self._main_chat_contact_label.set_label(f"{self._current_selected_nodeid} [CONNECTED]")
            else:
                self._main_chat_contact_label.set_label(f"{self._current_selected_nodeid} [OFFLINE]")
        else:
            self.main_text_view.set_buffer(self._dummy_txt_buffer)
            self._main_textentry.set_editable(False)
            self._main_chat_contact_label.set_label("<Select a contact first>")

    #
    # The following callbacks come from QUICCallbacks. These callbacks are used to
    # pass messages back to the UI from the QUIC/Websocket layer.
    #

    def _pre_incoming_connection(self, pub_key) -> bool:
        # Ensure this contact is in our contact store
        if contact := Contact.select().where(Contact.signature == pub_key.signature).first():
            if self._protocol.by_nodeid.get(contact.nodeid):
                self._send_notification(
                    f"⚠️ Rejecting duplicated connection request for contact: {contact.username}",
                    "There's already an active connection to this peer.",
                )
                return False
            # This is a genuinely new incoming connections
            return True

        self._send_notification(
            "⚠️ Blocked unknown connection request",
            f"Someone tried to connect, but they aren't in your contact database. Signature: {pub_key.signature.hex()}",
        )
        log.error("Blocked unknown connection request (cert signature: %s)", pub_key.signature.hex())
        return False

    def _connection_established(self, proto_class):
        contact = Contact.select().where(Contact.nodeid == proto_class.nodeid).first()
        assert contact, "Fatal runtime error: DB corruption error"  # bail out
        proto_class.contact_instance = contact
        self._send_notification(
            f"ℹ️ {contact.username}",
            f"Successful connection with this node at address {proto_class._client[0]} on port {proto_class._client[1]}",
        )

    def _incoming_message(self, proto_class, message):
        if self._gtk_text_buffers.get(proto_class.nodeid):
            db_msg = Conversation.create(contact=proto_class.contact_instance, message=message)
            txbuff_insert = f"[{proto_class.contact_instance.username}/{db_msg.date.strftime('%d %H:%M:%S')}] {message}"
            GLib.idle_add(
                self._insert_text_at_buffer,
                proto_class.nodeid,
                txbuff_insert,
            )

    def _connection_closed(self, proto_class):
        if proto_class._client:
            self._send_notification(
                "Close Event",
                f"Closed connection with peer at {proto_class._client[0]} on port {proto_class._client[1]}",
            )

    #
    # END QUICCallbacks
    #

    def _init_dhtpeers_view(self):
        for node in DHTNode.select():
            self._dhtpeers_store.append(
                [node.id, node.addr, node.port, "Yes" if node.is_manual else "Discovered via DHT"]
            )

    def _update_dhtpeers_view(self, *_args, **_kwargs):
        """
        Updates the DHT peer view.

        The new nodes we need to add to the local
        node database are defined by:

        {NEW_NEIGHBORS} - {EXISTENT_NEIGHBORS}
        """
        bootstrappable_neighbors = set(self._protocol.dht.bootstrappable_neighbors())
        # i[address], i[port]
        db_bootstrap_neighbors = set((node[1], node[2]) for node in self._dhtpeers_store)
        for node in bootstrappable_neighbors - db_bootstrap_neighbors:
            if db_obj := DHTNode.new_from_tuple(address=node[0], port=node[1]):
                self._dhtpeers_store.append([db_obj.id, db_obj.addr, int(db_obj.port), "Discovered via DHT"])

        dht_peers = self._protocol.dht.bootstrappable_neighbors()
        self._change_headerbar_subtitle(f"DHT: {len(dht_peers)} peers")

    def _update_contacts_view(self):
        # 1 is for DHT ID column (username, nodeid, status)
        ui_node_ids = [row[1] for row in self.contacts_treeview.get_model()]
        for contact in Contact.select():
            if contact.nodeid not in ui_node_ids:
                self.builder.get_object("contacts_store").append([contact.username, contact.nodeid])

    def _on_page_switch(self, _notebox, _box, page):
        """
        Update contact tab accordingly
        """
        # Page 1 is for contacts tab
        # Page 2 is DHT peers tab
        if page == 1:
            self._update_contacts_view()
        elif page == 2:
            self._update_dhtpeers_view()

    async def _async_on_connect_button_press(self, node) -> None:
        self._quic_config.load_verify_locations(node.pub_cert_path)  # load remote cert
        if ip_address := await self._protocol.find_by_cert(node.pub_cert):
            if not await self._protocol.connect(ip_address, node.signature):
                GLib.idle_add(
                    simple_dialog,
                    self.window,
                    f"Found '{node.username}', but unable to connect",
                    "You're possibly not in remote's contact book or they're impersonating your contact.",
                )
        else:
            GLib.idle_add(
                simple_dialog,
                self.window,
                f"Couldn't locate '{node.username}' on DHT",
                "DHT search didn't return any IP address for this node. Maybe they are offline?",
            )

    def _on_connect_button_press(self, *_args, **_kwargs):
        # Connect button event handler
        model, iterator = self.contacts_treeview.get_selection().get_selected()
        if iterator:
            contact = Contact.select().where(Contact.nodeid == model[iterator][1]).first()
            if self._protocol.by_nodeid.get(contact.nodeid):
                simple_dialog(self.window, "Already connected", f"You're already connected to '{contact.username}'")
            else:
                asyncio.run_coroutine_threadsafe(self._async_on_connect_button_press(contact), self._loop)
        else:
            simple_dialog(self.window, "Nothing is selected.", "Select a peer to connect.")

    def _on_main_textentry(self, *_args, **_kwargs):
        # entry text handler
        if text := self._main_textentry.get_text():
            if self._current_selected_nodeid:
                if conn := self._protocol.by_nodeid.get(self._current_selected_nodeid):
                    channel = conn.named_websockets.get(0b00000010)
                    channel.send_data(text.encode())
                    db_msg = Conversation.create(
                        contact=conn.contact_instance,
                        message=text,
                        is_local=True,
                    )
                    txbuff_insert = f"{db_msg.message}\t[You/{db_msg.date.strftime('%d %H:%M:%S')}]"
                    self._insert_text_at_buffer(self._current_selected_nodeid, txbuff_insert, is_local=True)
                    self._main_textentry.set_text("")  # clear text entry box

    def _on_add_new_contact(self, *_args, **_kwargs):
        # cert picker button
        if username := self.add_contact_username_entry.get_text():
            if absolute_path := self.contact_filechooser.get_filename():
                result, value = Contact.add_contact(self._args.certdir, username, absolute_path)
                if result:
                    self._contact_store.append([value.username, value.nodeid])
                    self._gtk_text_buffers[value.nodeid] = Gtk.TextBuffer.new(self._tag_table)
                    simple_dialog(
                        self.add_contact_window,
                        f"{value.username} added successfully",
                        f"Signature: {value.signature.hex()}\n\nSHA-1/NodeID: {value.nodeid}",
                    )
                    self.add_contact_window.hide()
                    self.add_contact_username_entry.set_text("")
                else:
                    simple_dialog(self.window, "An error has occurred", value)
                self.contact_filechooser.unselect_all()
            else:
                simple_dialog(
                    self.add_contact_window,
                    "Please select a valid certificate file",
                    "Needed in order to extract the public key and some other useful information.",
                )
        else:
            simple_dialog(
                self.add_contact_window,
                "You must enter an username",
                "You don't want to be unable to identify a contact, do you?",
            )

    def _on_add_new_node(self, *_args, **_kwargs):
        if ip_address := self.add_new_peer_address.get_text():
            port = self.add_new_peer_port.get_text()
            try:
                addr, port = ipv4_2_ipv6((ip_address, port))
                if not DHTNode.select().where(DHTNode.addr == addr, DHTNode.port == port).first():
                    db_obj = DHTNode.create(addr=addr, port=port, is_manual=True)
                    simple_dialog(
                        self.add_contact_window,
                        "Entry node added successfully",
                        f"IP address: {addr} on port {port}",
                    )
                    self._dhtpeers_store.append([db_obj.id, db_obj.addr, int(db_obj.port), "Yes"])
                    self.add_node_window.hide()
                    self.add_new_peer_address.set_text("")
                    asyncio.run_coroutine_threadsafe(self._retrigger_bootstrapping(), self._loop)
                else:
                    simple_dialog(
                        self.add_contact_window,
                        "This DHT node is already on database.",
                        "They are already used as an entry node to the DHT.",
                    )
            except Exception as err:
                simple_dialog(
                    self.add_contact_window,
                    "Invalid IP address",
                    f"Couldn't parse IP address: {err}",
                )
        else:
            simple_dialog(
                self.add_contact_window,
                "Please enter a valid IP address (and port).",
                "This software can't magically join the DHT with a blank IP address.",
            )

    def _dhtnode_remove_selected(self, _widget):
        """
        Deletes a DHT bootstrap node both from database and from
        the DHT peers tab.
        """
        model, iterator = self.dht_peers_treeview.get_selection().get_selected()
        if iterator:
            dht_db_id = model[iterator][0]  # first entry has id from database
            if db_obj := DHTNode.select().where(DHTNode.id == dht_db_id).first():
                db_obj.delete_instance()
                del model[iterator]

    def _contacts_remove_selected(self, _widget):
        model, iterator = self.contacts_treeview.get_selection().get_selected()
        if iterator:
            node_id = model[iterator][1]
            if confirm_dialog(
                self.window,
                "Are you really sure you want to delete this contact?",
                "You'll need their public cert again. Active connections will be closed.",
            ):
                self.main_text_view.set_buffer(self._dummy_txt_buffer)
                if contact := Contact.select().where(Contact.nodeid == node_id).first():
                    contact.delete_instance()
                del self._gtk_text_buffers[node_id]  # delete TextBuffer
                del model[iterator]  # delete row from contacts view
                if conn := self._protocol.by_nodeid.get(node_id):
                    conn.close()
        else:
            simple_dialog(self.window, "Nothing is selected.", "Select a contact to delete.")

    def _send_notification(self, title: str, message: str):
        if self.notifications_switch.get_active():
            GLib.idle_add(simple_notify, title, message)

    def _on_about_press(self, _widget):
        self.about_window.run()
        self.about_window.hide()

    def die(self, *_args, **_kwargs):
        """
        Die gracefully.
        """
        self._protocol.close()  # terminate all connections
        self._loop.call_soon_threadsafe(self._loop.stop)  # stop event loop
        self._thread_loop.join()  # join event loop thread
        Gtk.main_quit()
        log.info("Goodbye YAECC.")
        return True


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="YAECC Server/Client")
    parser.add_argument("--sk", type=str, required=True, help="private key")
    parser.add_argument("--pk", type=str, required=True, help="x509 server public key")
    parser.add_argument("--port", type=int, required=True, help="local serving & listening port")
    parser.add_argument("--certdir", type=str, required=True, default="peer_certs", help="X509 cert directory")
    parser.add_argument(
        "--contactdb", type=str, required=True, default="storage.sqlite3", help="Storage database (sqlite3)"
    )
    args = parser.parse_args()

    main = MainWindow(args=args)
    signal.signal(signal.SIGINT, main.die)
    signal.signal(signal.SIGTERM, main.die)
    Gtk.main()
