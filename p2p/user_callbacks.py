import logging

log = logging.getLogger(__name__)


class QUICCallbacks:
    def pre_incoming_connection(self, pub_key) -> bool:
        """
        Pre-connect callback.

        Parameters
        ----------
        pub_key: cryptography.x509.Certificate
            Public x509 certificate.

        Returns
        -------
        bool
            True: Allow this connection to continue. Otherwise, close it.
            False: Close immediately this connection.

        Notes
        -----
        This class will return True by default (allow all incoming connections).
        """
        log.info("[callback] pre_incoming_connection: %s", pub_key)
        return True

    def connection_established(self, proto_class) -> None:
        """Fired whenever a connection has been completely established.

        Parameters
        ----------
        proto_class: QuicConnectionProtocolCB
            An instance of the connection.

        Notes
        -----
        This callback will be called in both servers and clients.
        For servers, this is called after the CTRL websocket channel has been established.
        For clients, this is called after all the communication channels have been opened.
        """
        log.info(
            "[callback] connection_established: NodeID=%s, cert signature=%s",
            proto_class.nodeid,
            proto_class.cert_signature.hex(),
        )

    def incoming_message(self, proto_class, message) -> None:
        log.info("[callback] incoming_message: NodeID=%s, msg=%s", proto_class.nodeid, message)

    def connection_closed(self, proto_class) -> None:
        log.info("[callback] connection_closed: client=%s, NodeID=%s", proto_class._client, proto_class.nodeid)
