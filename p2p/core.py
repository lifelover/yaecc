"""
The very heart of YAECC.
"""
import asyncio
import logging
import secrets
from functools import partial
from hashlib import sha1
from typing import Callable, Dict, Optional, Text, Union

from aioquic.asyncio.protocol import QuicConnectionProtocol, QuicStreamHandler
from aioquic.buffer import Buffer
from aioquic.quic.configuration import QuicConfiguration
from aioquic.quic.connection import NetworkAddress, QuicConnection
from aioquic.quic.packet import (
    PACKET_TYPE_INITIAL,
    encode_quic_retry,
    encode_quic_version_negotiation,
    pull_quic_header,
)
from aioquic.quic.retry import QuicRetryTokenHandler
from cryptography.hazmat.primitives.serialization import Encoding
from cryptography.x509.oid import SignatureAlgorithmOID
from kademlia.protocol import KademliaProtocol
from kademlia.utils import digest

from monkeypatch import kademlia_patch, aioquic_patch

handler = logging.StreamHandler()
kadlog = logging.getLogger("kademlia")
kadlog.addHandler(handler)
kadlog.setLevel(logging.WARNING)
kademlia_patch.patch()
aioquic_patch.patch()

from dht.server import EnhancedKademliaServer
from utils import ip

log = logging.getLogger(__name__)


class P2PQuicHandler(asyncio.DatagramProtocol):
    def __init__(
        self,
        *,
        configuration: QuicConfiguration,
        callback_cls,
        create_protocol: Callable = QuicConnectionProtocol,
        create_protocol_client: Callable = QuicConnectionProtocol,
        stream_handler: Optional[QuicStreamHandler] = None,
    ) -> None:
        self._configuration = configuration
        self._callback_cls = callback_cls
        self._create_protocol = create_protocol
        self._create_protocol_client = create_protocol_client
        self._loop = asyncio.get_event_loop()
        self._protocols: Dict[bytes, QuicConnectionProtocol] = {}
        self.by_nodeid: Dict[Text, QuicConnectionProtocol] = {}
        self._transport: Optional[asyncio.DatagramTransport] = None

        self._stream_handler = stream_handler
        self._retry = QuicRetryTokenHandler()
        # Public and private keys of this instance (only clients use this)

        self._pkey_bytes = self._configuration.certificate.public_bytes(Encoding.PEM)
        self._skey = self._configuration.private_key

        # DHT instance
        self._dht_id = digest(self._pkey_bytes)
        self.dht: EnhancedKademliaServer = EnhancedKademliaServer(node_id=self._dht_id)
        log.info("DHT: local NodeID: %s", self._dht_id.hex())

        # Create future task in order to prevent pushing to the DHT
        # without previous bootstrapping
        self._is_bootstrapping_done = self._loop.create_future()

    def _bind_dht(self, transport):
        """
        Spin up DHT instance without using another datagram protocol
        """
        self.dht.transport = transport
        self.dht.protocol = KademliaProtocol(self.dht.node, self.dht.storage, self.dht.ksize)
        self.dht.protocol.connection_made(transport)
        self.dht.refresh_table()

    async def bootstrap_async(self, nodes: list):
        """
        Bootstraps against a given list of nodes.

        Parameters
        ----------
        nodes: list[tuple[str, int]]
            List of nodes in the form [(ip, port), ...]

        Returns
        -------
        int
            Count of successfully contacted and bootstrapped nodes.
        """
        log.info("DHT: bootstrapping with %s known nodes", len(nodes))
        # Bootstrap against multiple nodes at the same time
        coros = [self.dht.bootstrap([node]) for node in nodes]
        bootstrapped_count = sum([1 for item in (await asyncio.gather(*coros)) if item])
        self._is_bootstrapping_done.set_result(True)
        log.info("DHT: bootstrapped with %s node(s)", bootstrapped_count)
        return bootstrapped_count

    async def _dht_set_id(self, frequency: int = 60 * 5) -> None:
        """
        Sends requests to the DHT network to
        publish our own public key from time to time.

        Notes
        -----
        dht.set(key, value) will automatically
        calculate the SHA1 hash for our key so we can
        pass it just as is.
        """

        # Avoid publishing stuff without DHT nodes
        await self._is_bootstrapping_done

        while True:
            await self.dht.set(self._pkey_bytes, self._pkey_bytes)
            log.info("DHT: local public cert broadcast")
            await asyncio.sleep(frequency)

    async def find_by_cert(self, pub_cert: bytes):
        """
        Attempts to find a node given its public x509 certificate.

        Parameters
        ----------
        pub_cert: bytes
            Raw remote's x509 public certificate.
        """

        # Wait until bootstrapping is complete so we can
        # crawl the DHT with an actual entry point
        await self._is_bootstrapping_done

        hasher = sha1()
        hasher.update(pub_cert)
        key = hasher.digest()

        log.info("DHT: searching NodeID %s", key.hex())
        if results := await self.dht.find_node(key=key):
            for node in results:
                if node.id == key:
                    log.info("DHT: found NodeID %s at %s:%s", key.hex(), node.ip, node.port)
                    return (node.ip, node.port)
        log.info("DHT: couldn't find NodeID %s", key.hex())
        return None

    def close(self):
        """Closes all existing connections, and then shuts the
        transport down
        """
        for protocol in set(self._protocols.values()):
            protocol.close()
        self._protocols.clear()
        self._transport.close()

    def connection_made(self, transport: asyncio.BaseTransport) -> None:
        """
        Only called once by asyncio's DatagramProtocol
        """
        self._transport = transport
        self._bind_dht(transport)
        self._loop.create_task(self._dht_set_id())
        # Put our own public key in the DHT

    def datagram_received(self, data: Union[bytes, Text], addr: NetworkAddress) -> None:
        buf = Buffer(data=data)

        try:
            header = pull_quic_header(buf, host_cid_length=self._configuration.connection_id_length)
        except ValueError:
            # received kademlia packet
            self._loop.create_task(self.dht.protocol._solve_datagram(data, addr))
            return

        if protocol := self._protocols.get(header.destination_cid):
            # received a packet for an already-established connection
            protocol.datagram_received(data, addr)
        # remote wants to connect to us using a wrong TLS version
        elif header.version is not None and header.version not in self._configuration.supported_versions:
            self._transport.sendto(
                encode_quic_version_negotiation(
                    source_cid=header.destination_cid,
                    destination_cid=header.source_cid,
                    supported_versions=self._configuration.supported_versions,
                ),
                addr,
            )
        elif header.packet_type == PACKET_TYPE_INITIAL and len(data) >= 1200:
            if not header.token:
                # create a retry token if the header doesn't already contain one
                source_cid = secrets.token_bytes(8)
                self._transport.sendto(
                    encode_quic_retry(
                        version=header.version,
                        source_cid=source_cid,
                        destination_cid=header.source_cid,
                        original_destination_cid=header.destination_cid,
                        retry_token=self._retry.create_token(addr, header.destination_cid, source_cid),
                    ),
                    addr,
                )
                return
            # validate retry token
            try:
                (
                    original_destination_connection_id,
                    retry_source_connection_id,
                ) = self._retry.validate_token(addr, header.token)
            except ValueError:
                log.info("address validation failed for %s", addr)
                return
            # create new connection
            connection = QuicConnection(
                configuration=self._configuration,
                original_destination_connection_id=original_destination_connection_id,
                retry_source_connection_id=retry_source_connection_id,
            )
            protocol = self._create_protocol(
                connection, callback_class=self._callback_cls, stream_handler=self._stream_handler
            )

            log.info("new connection from %s, id=%s", addr, protocol._id)
            self._set_proto_callbacks(protocol)
            protocol.connection_made(self._transport)

            # register callbacks for connection id-related changes

            self._protocols[header.destination_cid] = protocol
            self._protocols[connection.host_cid] = protocol

    def _set_proto_callbacks(self, protocol) -> None:
        """Adds callbacks for connection id QUIC events.

        Most of these aren't even probably going to be used in YAECC, but
        there's no harm in supporting them.

        Parameters
        ----------
        protocol: QuicConnectionProtocol
            A connection protocol.
        """
        setattr(
            protocol,
            "_connection_id_issued_handler",
            partial(self._connection_id_issued, protocol=protocol),
        )
        setattr(
            protocol,
            "_connection_id_retired_handler",
            partial(self._connection_id_retired, protocol=protocol),
        )
        setattr(
            protocol,
            "_connection_terminated_handler",
            partial(self._connection_terminated, protocol=protocol),
        )
        if protocol.is_incoming:
            # Inject callback into authenticator after class has been
            # initialized
            setattr(
                protocol._auth,
                "register_nodeid",
                partial(self._register_nodeid, protocol=protocol),
            )

    def _register_nodeid(self, nodeid: str, protocol: QuicConnectionProtocol) -> None:
        self.by_nodeid[nodeid] = protocol

    def _connection_id_issued(self, cid: bytes, protocol: QuicConnectionProtocol) -> None:
        self._protocols[cid] = protocol

    def _connection_id_retired(self, cid: bytes, protocol: QuicConnectionProtocol) -> None:
        assert self._protocols[cid] == protocol
        del self._protocols[cid]

    def _connection_terminated(self, protocol: QuicConnectionProtocol) -> None:
        protocol.clean_websockets()
        self._callback_cls.connection_closed(protocol)

        for cid, proto in list(self._protocols.items()):
            if proto == protocol:
                del self._protocols[cid]

        if protocol.nodeid in self.by_nodeid:
            del self.by_nodeid[protocol.nodeid]

    async def connect(self, addr, signature=str, timeout=10) -> bool:
        """Connects to a remote peer

        Parameters
        ----------
        addr: tuple
        Tuple containing (ip_addr, port)

        """
        self._configuration.is_client = True
        connection = QuicConnection(configuration=self._configuration)
        self._configuration.is_client = False
        protocol = self._create_protocol_client.as_client(
            connection,
            callback_class=self._callback_cls,
            stream_handler=self._stream_handler,
        )
        protocol.set_keypairs(self._pkey_bytes, self._skey)
        self._protocols[connection.host_cid] = protocol
        self._set_proto_callbacks(protocol)
        protocol.connection_made(self._transport)
        protocol.connect(ip.ipv4_2_ipv6(addr))
        try:
            await asyncio.wait_for(protocol.wait_connected(), timeout=timeout)
            protocol.init_remote_attrs()

            # For client-initiated connections, we'll need to enforce TLS connections
            # only over Ed25519 for public-key as signature algorithm
            if protocol._remote_cert.signature_algorithm_oid == SignatureAlgorithmOID.ED25519:
                # Ensure remote signature matches our stored signature
                if protocol.cert_signature == signature:
                    self._register_nodeid(str(protocol.nodeid), protocol)
                    return await protocol.try_setup()
                log.error("[%s] bad signature, closing")
            log.error("TLS setup error (DSA: %s)", protocol.remote_cert.signature_algorithm_oid)
            protocol.close()

        except (ConnectionError, asyncio.TimeoutError):
            log.info("QUIC: timed out connecting to %s", addr)
            protocol.close()
