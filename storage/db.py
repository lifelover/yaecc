"""
Database manager.
"""
import datetime
import logging
from hashlib import sha1
import pathlib
from uuid import uuid4

from cryptography.x509 import load_pem_x509_certificate
from peewee import (
    Model,
    AutoField,
    DatabaseProxy,
    ForeignKeyField,
    CharField,
    BlobField,
    DateTimeField,
    BooleanField,
    IntegerField,
    UUIDField,
    SqliteDatabase,
)

logging.basicConfig(level=logging.INFO, format="%(asctime)s [%(levelname)s] %(message)s")
log = logging.getLogger(__name__)


def sha1digest(data) -> bytes:
    """
    Calculate the SHA-1 hash of a bytearray
    """
    hasher = sha1()
    hasher.update(data)
    return hasher.digest()


class Base(Model):
    class Meta:
        database = DatabaseProxy()


class Contact(Base):
    id = AutoField()
    username = CharField()
    nodeid = CharField(unique=True)
    pub_cert = BlobField()
    pub_cert_path = CharField()
    signature = BlobField()

    @classmethod
    def add_contact(cls, cert_dir, username: str, cert_path: str) -> tuple:
        """
        Adds a contact, given their username and pub cert

        Parameters
        ----------
        username: str
        cert_path: str

        Returns
        -------
        Tuple[Bool, Union[Contact,str]]
            Tuple[True, Conctact] whenever we've been able to store a new contact successfully.
            Tuple[False, str] whenever something wrong happened when saving a contact.

        """
        cert_dir = pathlib.Path(cert_dir)
        cert_dir.mkdir(parents=True, exist_ok=True)

        with open(cert_path, "rb") as handle:
            contents = handle.read()
            try:
                cert = load_pem_x509_certificate(contents)
            except ValueError:
                log.error("Unable to load certificate (load_pem_x509_certificate)")
                return (False, "Call to load_pem_x509_certificate() failed, maybe this file isn't in X509 format?")
            key = sha1digest(contents).hex()
            if cls.select().where(cls.nodeid == key):
                return False, "Certificate already exists in local cert database."

            path = cert_dir / f"{key}.crt"
            contact = cls(
                username=username,
                nodeid=key,
                path=path,
                pub_cert=contents,
                pub_cert_path=path,
                signature=cert.signature,
            )
            contact.save()
            with open(path, "wb") as handle:
                handle.write(contents)

            log.info("ContactStorage: %s added", key)

            return True, contact


class Conversation(Base):
    id = UUIDField(primary_key=True, default=uuid4)
    contact = ForeignKeyField(Contact, on_delete="CASCADE")
    message = CharField()
    date = DateTimeField(default=datetime.datetime.now)
    # whether this message was sent from us
    is_local = BooleanField(default=False)


class DHTNode(Base):
    addr = CharField()
    port = IntegerField()
    is_manual = BooleanField(default=False)

    @classmethod
    def new_from_tuple(cls, address: str, port: int):
        """
        Creates a new instance of DHTNode, but checks
        first if the tuple already exists on database.
        """
        port = str(port)
        if not cls.select().where(cls.addr == address, cls.port == port).exists():
            return cls.create(addr=address, port=port)
        return None

    @classmethod
    def get_as_tuples(cls) -> list:
        return [(node.addr, node.port) for node in cls.select()]


ALL_MODELS = [Contact, Conversation, DHTNode]


class DBInit:
    """
    Nice wrapper only used to initialize the local database.
    """

    def __init__(self, filename: str, restore_path) -> None:
        self._restore_path = pathlib.Path(restore_path)
        self._restore_path.mkdir(parents=True, exist_ok=True)
        Base._meta.database.initialize(
            SqliteDatabase(filename, pragmas={"journal_mode": "memory", "foreign_keys": "on"})
        )
        Base._meta.database.connect()
        Base._meta.database.create_tables(ALL_MODELS)
        self._restore_certs()

    def _restore_certs(self):
        """
        Restores public node certs from the database. This can
        happen if the end user deletes their certificate folder.

        If the user deleted their .sqlite3 database, then we're pretty
        much screwed up and we won't do anything at all.
        """
        cert_dir = pathlib.Path(self._restore_path)
        cert_dir.mkdir(parents=True, exist_ok=True)

        log.info("ContactStorage: cert database: loaded")
        for contact in Contact.select():
            cert_filename = self._restore_path / f"{contact.nodeid}.crt"
            if not pathlib.Path(cert_filename).is_file():
                with open(cert_filename, "wb") as wr_handle:
                    wr_handle.write(contact.pub_cert)
                log.warning("ContactStorage: restoring cert for NodeID %s", contact.nodeid)
