import logging

from kademlia.crawling import NodeSpiderCrawl
from kademlia.network import Server
from kademlia.node import Node
from kademlia.utils import digest

log = logging.getLogger("kademlia")


class EnhancedKademliaServer(Server):
    """
    This is just an slightly enhanced version of
    kademlia's DHT server. The original Server doesn't implement
    search for nodes.
    """

    async def find_node(self, key: bytes):
        """
        Find the closest nodes to a given key
        """
        node = Node(digest(key))
        nearest = self.protocol.router.find_neighbors(node)
        if not nearest:
            log.warning("There are no known neighbors to get key %s", key)
            return None
        spider = NodeSpiderCrawl(self.protocol, node, nearest, self.ksize, self.alpha)
        return await spider.find()
